class Certificat:
    capaciterDunSac: int
    nombreDeSac: int
    nbObjet: int
    # A chaque index du tableau i correspond le num d'un objet on associe le numero d'un sac de 0 a nbSac -1
    solution: [int]

    def __init__(self, nombreDeSac: int, capaciterDunSac: int, nbObjet: int, solution: [int]) -> None:
        self.nombreDeSac = nombreDeSac
        self.nbObjet = nbObjet
        self.solution = solution
        self.capaciterDunSac = capaciterDunSac

    def certificatCheck(self, poidDesObjets: [int]) -> bool:
        sac = [0] * self.nombreDeSac
        for k in range(self.nbObjet):
            sac[self.solution[k]] += poidDesObjets[k]
            if sac[self.solution[k]] > self.capaciterDunSac:
                return False
        return True
