from Partition import Partition


class Sum:
    nbEntier: int
    listeEntiers: [int]
    cible: int

    def __init__(self, nbEntier: int = -1, cible: int = -1, listEntiers: [int] = []) -> None:
        self.nbEntier = nbEntier
        self.listeEntiers = listEntiers
        self.cible = cible

    def initialise(self, cheminFichierTest):
        file = open(cheminFichierTest, "r")
        all = file.readlines()
        n = int(all[0])
        lsent = []
        for i in range(n):
            lsent.append(int(all[i + 1]))
        self.cible = int(all[n + 1])
        self.nbEntier = n
        self.listeEntiers = lsent

    def afficherSum(self):
        print(f"Partition: \n nombre d'entier: {self.nbEntier}  cible: {self.cible}    liste des entiers: "
              f"{self.listeEntiers} ")

    def reduceToPartition(self) -> Partition:
        ls = self.listeEntiers + [self.cible]
        return Partition(self.nbEntier + 1, ls)
