# BinPack

## 1. Qu’est-ce qu’une propriété NP ?
### Q1 : Propriété NP

*On a un probleme K. Un certificat C est une representation d'une proposition solution de K. Un certificat peut etre
 vrai ou faux suivant si il est réelement solution du probleme K. Il contient égalment un algorithme qui verifi en temps
polinomiale n certificar.

* Le certificat sera représenter par un tableau de taille n, ou a chaque indice du tableau correspond un objet et a cette
indice ce trouve le numero d'un sac dans le quel il sera stocker.

* Un certifacat fera une taille n, n etant le nombre d'objet du probleme. Le certificta est donc bien borner par la taille
des donné d'entré du pb. Car les donnés d'entré u { t , [objs] } avec t la taille d'un sac et [objs] la liste des poids des objets.

```py
    def certificatCheck(self, poidDesObjets: [int]) -> bool:
        sac = [0] * self.nombreDeSac
        for k in range(self.nbObjet):
            sac[self.solution[k]] += poidDesObjets[k]
            if sac[self.solution[k]] > self.capaciterDunSac:
                return False
        return True
```
* Le code ci-dessu est la proposition de notre algoritme de vérification il est polynomiale et dépendant de la taille 
de la solution (ici, le nombre d'objet a mettre dans les sacs).

### Q2 : NP = Non déterministe polynomial

* L'algorithme suivant génére des certifacats et chaque certificat a une probabiliter non null d'apparaitre.
Cepandant la il ne génére pas les certificats de maniére uniforme. La distribution de la génération est une gaussienne.

```py    
    def genereAleatoireCetificat(self, nbSac: int):
        sol = [0] * self.nbObjet
        for k in range(self.nbObjet):
            sol[k] = pif.randrange(0, nbSac)
        certif = Certificat(nbSac, self.capaciterDunSac, self.nbObjet, sol)
        return certif
```

* On génre au hazard un certificat puis pour chaque certificat généré ainsi on verifie si il est solution ou non du probleme
    si il est solution on s'arrete est on l'affiche sinon on continue. Cette algorithme serais non-deterministe car génére
    des certificats alléatoirement et sa condition d'arret serais fixé a nombre d'itération choisie a l'avance.


### Q3 : NP ⊂ EXPTIME 

* Si n, le nombre d'objet et k le nombre de sac son fixé alors la solution ne dépend plus que de c la capaciter d'un sac 
et des différentes combinaison (objet, sac). Soit a chaque valeur possible de capaciter d'un sac c et avec n objets on a
un tableau de taille n at qui a chaque idice aune valeur de 0 a k-1.

* Nous allons enumerer les tableau possible en commencant par le tableau [0,0,0,0, ... , n-1] et fini par 
[k-1,k-1,k-1, ... n-1] et en fesant évoluer le nombre de sac.

* Le probleme a une solution ssi la capaciter d'un sac est supérieur ou égale au poid de l'objet le plus lourd.
 Pour obtenir un maximum on a un algo en n car notre liste non trié.

```
    def aUneSolution(self):
        return max(self.poidsDesObjets) <= self.capaciterDunSac
```

### Q4 : Implémentation

* Vous verrait dans le fichier main.py que le programme est sparé par des commentaire qui vous permetrons de vous reperer
 dans le code.
 
## 2.  Reductions polynomiales

>Q1 : Montrer que P artition se réduit polynomialementen BinPack.
>
>* Cf le code :
```py
    def reduceToBinPack(self) -> BinPack:
        nbSac = 2
        capaSac = sum(self.listeEntiers) // 2
        return BinPack(self.nbEntier, self.listeEntiers, capaSac, nbSac)
```
>
>* On sait que BinPack est NP et que Partition et NP-complet. On a prouver Q1.1 que Partition se réduiser en BinPack.
>On peut en déduire que BinPack est NP et NP-dure soite que BinPack est NP-complet.
>
>* Partition est NP-complet donc est NP-dure, BinPack étant NP alors par definition BinPack peut se réduire en Partition.

### Une réduction un peu moins évidente

>Q2 : Entre Sum et P artition, lequel des deux problèmes peut être presque vu comme un cas particulier de l’autre ? 
>Qu’en déduire en terme de réduction ?
>
>* Sum semble etre un sous probleme de Partition. En effet dans partition on cherche a savoir si la sum d'une suite de 
>chiffres est égale a une autre et dans Sum on ne réalise la moitier du probleme car on cherche si une suite de chiffres
>est égale a un chiffre unique.
>
>Q3 : Montrer que Sum se réduit polynomialement en P artition et implémentez la réduction.
>
```py
    def reduceToPartition(self) -> Partition:
        return Partition(self.nbEntier + 1, self.listeEntiers.append(self.cible))
```
### Composition de réductions

>Q4 : En utilisant la réduction précédente, comment implémenter une réduction polynomiale de Sum dans BinPack ?
>
>* Pour cela nous passerons par la reduction de Sum en Partition. Sum -> Partition puis Partition -> BinPack

### Une dernière réduction

>Q5 : Proposer une réduction polynomiale de BinPackDiff dans BinPack.
>
>* Une possibiliter serais de dire que chaque capaciter maximal d'un sac (cpM) du probleme binPack serais la valeur de 
>capaciter la plus petite du probleme binPackDiff. Dans ce cas la si on trouve une solution on poura dire qu'il existe
> une mise en sachet fonctionnel avec des sac qui on une capaciter d'au moins cpM ce qui réponderais au probleme 
> binPackDiff. Cepandant cette reduction ne couvre pas la totaliter des configuration de binPackDiff, en effet si un 
> objet a un poid de 10 et que la capaciter minimum d'un sac est de 5 sa ne signifie pas que l'objet ne rentreré pas 
>dans un sac de capaciter supérieur.
>* Une autre methode serais de diviser le probleme binPackDiff en plusieur sous probleme binPack en regroupant les sacs
>de meme capaciter. Ce qui pose probleme ici serais la répartition des objets. On pourrais arbitrairement les répartirs
> en appliquant que seul les objets de poids inferieur a la taille du sac sois utiliser pour le probleme courant mais
>cela impliqueré un post-traitement aprés la résultion des sous probleme binPack pour recombiner les résultat.


## 3. Optimisation versus Décison

On rappelle :

>BinPack  
&emsp;          Donnée :  
&emsp;&emsp;        n –un nb d’objets  
&emsp;&emsp;        x1, · · · , xn – n entiers, les poids des objets  
&emsp;&emsp;        c –la capacité d’un sac (entière)  
&emsp;&emsp;        k –le nombre de sacs  
&emsp;          Sortie :  
&emsp;&emsp;        Oui, si il existe une mise en sachets possibles, i.e. :  
&emsp;&emsp;&emsp;            aff : [1..n] → [1..k] –à chaque objet, on attribue un numéro d'un sac  
&emsp;&emsp;&emsp;            tq $ \sum i/aff(i)=j  $ $ x_i \leq c $ , pour tout numéro de sac j, 1 ≤ j ≤ k. – aucun sac n’est trop chargé  
&emsp;&emsp;        Non, sinon


>BinPackOpt1  
&emsp;  Donnée :  
&emsp;&emsp;    n –un nb d’objets  
&emsp;&emsp;    x1, · · · , xn – n entiers, les poids des objets  
&emsp;&emsp;    c –la capacité d’un sac (entière)  
&emsp;  Sortie : le nombre minimal de sachets nécessaires pour la mise en sachets  


>BinPackOpt2  
&emsp;  Donnée :  
&emsp;&emsp;    n –un nb d’objets  
&emsp;&emsp;    x1, · · · , xn – n entiers, les poids des objets  
&emsp;&emsp;    c –la capacité d’un sac (entière)  
&emsp;&emsp;    k –le nombre de sacs  
&emsp;  Sortie : Une mise en sachets correcte qui minimise le nombre de sachets.  

#### Q1 : Montrer que si BinPackOpt1 (resp. BinPackOpt2) était P, la propriété BinPack le serait aussi. Qu’en déduire pour BinackOpt1 (resp. BinPackOpt2) ?

* On sait ici que BinPack est NP-dure. Si on affirme que BinPackOpt1 (resp. BinPackOpt2) sont des problemes d'optimisation qui
découle de BinPack et on suppose que ces problemes d'optimisation sont P alors BinPack serais P. En effet a tout probleme 
d'optimisation qui minimise ou maximise une donnée on peut associer une question "existe-t-il une solution de coût 
inférieur (resp. de gain supérieur) à une valeur donnée en entrée?". On en déduit ici que si le probleme de décision est
NP-dure alors le probleme d'optimisation le sera aussi. Soit BinPackOpt1 (resp. BinPackOpt2) est NP-dure
  
#### Q2 et Q3: Montrer que si la propriété BinPack était P, BinPackOpt1 le serait aussi. Montrer que si la propriété BinPack était P, BinP ackOpt2 le serait aussi.

* De la meme maniére que pour la Q1 Si on suppose quel'on peut résoudre BinPack en P alors les problemes d'optimisation
 qui en découle pourons égalment etre résolut en P.
