class BinPack:
    nbObjet: int
    poidsDesObjets: [int]
    capaciterDunSac: int
    nombreDeSac: int

    def __init__(self, nbObject: int = -1, poidsDesObjets: [int] = [], capaciterDunSac: int = -1,
                 nombreDeSac: int = -1) -> None:
        self.nbObjet = nbObject
        self.poidsDesObjets = poidsDesObjets
        self.capaciterDunSac = capaciterDunSac
        self.nombreDeSac = nombreDeSac

    def initialise(self, cheminFichierTest):
        file = open(cheminFichierTest, "r")
        all = file.readlines()
        c = int(all[0])
        n = int(all[1])
        pdobj = []
        for i in range(n):
            pdobj.append(int(all[i + 2]))
        self.nbObjet = n
        self.poidsDesObjets = pdobj
        self.capaciterDunSac = c

    def aUneSolution(self):
        return max(self.poidsDesObjets) <= self.capaciterDunSac

    def afficherBinPack(self):
        print(f"  Nombre d'objets = {self.nbObjet}, Nombre de sac {self.nombreDeSac}, Capaciter d'un sac "
              f"{self.capaciterDunSac}\n  Poid des objets : {self.poidsDesObjets}\n")
