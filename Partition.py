from BinPack import BinPack


class Partition:
    nbEntier: int
    listeEntiers: [int]

    def __init__(self, nbEntier: int = -1, listEntiers: [int] = []) -> None:
        self.nbEntier = nbEntier
        self.listeEntiers = listEntiers

    def initialise(self, cheminFichierTest):
        file = open(cheminFichierTest, "r")
        all = file.readlines()
        n = int(all[0])
        lsent = []
        for i in range(n):
            lsent.append(int(all[i + 1]))
        self.nbEntier = n
        self.listeEntiers = lsent

    def afficherPartition(self):
        print(f"Partition :\n nombre d'entier : {self.nbEntier}     liste des entiers : {self.listeEntiers} ")

    def reduceToBinPack(self) -> BinPack:
        nbSac = 2
        capaSac = sum(self.listeEntiers) // 2
        return BinPack(self.nbEntier, self.listeEntiers, capaSac, nbSac)
