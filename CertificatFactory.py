from Certificat import Certificat
import random as pif


class CertificatFactory:
    cpt: int
    nbSacCourant: int
    nbSacMaxi: int
    certifCourant: [int]
    capaciterDunSac: int
    nbObjet: int

    def __init__(self, capaciterDunSac: int, nbObjet: int) -> None:
        self.capaciterDunSac = capaciterDunSac
        self.cpt = 0
        self.nbSacCourant = 1
        self.nbObjet = nbObjet
        self.certifCourant = [0] * nbObjet

    def certificatSuivant(self) -> Certificat:
        if self.nbSacCourant == 1:
            self.nbSacCourant += 1
            return Certificat(self.nbSacCourant, self.capaciterDunSac, self.nbObjet, [0] * self.nbObjet)

        valMaxInBase = (self.nbSacCourant ** self.nbObjet) - 1
        if valMaxInBase < self.cpt:
            self.nbSacCourant += 1
            self.cpt = 0

        sol = self.valueInBase(self.cpt, self.nbSacCourant)
        self.cpt += 1

        certif = Certificat(self.nbSacCourant, self.capaciterDunSac, self.nbObjet, sol)
        return certif

    def isMax(self) -> bool:
        return self.nbSacCourant > self.nbSacMaxi

    def genereAleatoireCetificat(self, nbSac: int):
        sol = [0] * self.nbObjet
        for k in range(self.nbObjet):
            sol[k] = pif.randrange(0, nbSac)
        certif = Certificat(nbSac, self.capaciterDunSac, self.nbObjet, sol)
        return certif

    def setNbSacMaxi(self, nbSacMaxi: int):
        self.nbSacMaxi = nbSacMaxi

    def valueInBase(self, v, b):
        sol = [0] * self.nbObjet
        for k in range(self.nbObjet - 1, -1, -1):
            sol[k] = v // (b ** k)
            v = v % (b ** k)
        return sol
