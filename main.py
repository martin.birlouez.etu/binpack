from BinPack import BinPack
from Process import Process

# lecture d'instance de binPack
binPack = BinPack()
try:
    print("Saisisez le chemin du fichier de test : exemple './dt1/exBPeq1_min3'")
    file = input()
    binPack.initialise(file)
except FileNotFoundError:
    print("Erreur de saisie du fichier de test\n")
    exit(1)
print("Le bin pack selectionner :\n")
binPack.afficherBinPack()

# Choix du mode
print("Choix du mode :\n  (1) verification   (2) non-deterministe   (3) exploration exhaustive\n")
try:
    choix = int(input())
except Exception:
    print("Erreur de saisie du choix du mode\n")
    exit(2)
if choix not in [1, 2, 3]:
    print("Erreur de saisie du choix du mode\n")
    exit(3)

pros = Process(binPack)

# mode vérification
if choix == 1:
    pros.verification()

# mode non-deterministe
if choix == 2:
    pros.nonDeterministe()

# mode exploration exaustive
if choix == 3:
    pros.explorationExaustive()
