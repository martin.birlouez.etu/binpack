from Certificat import Certificat
from CertificatFactory import CertificatFactory
from BinPack import BinPack


class Process:
    binPack: BinPack

    def __init__(self, binPack) -> None:
        self.binPack = binPack

    def verification(self):
        print("Vous avez choisie Mode : verification\n")
        print(
            f"Le Certifact correspondant au binPack choisie est :\n Nombre d'objet : {self.binPack.nbObjet},"
            f" Capaciter d'un sac : {self.binPack.capaciterDunSac}\n")
        nbsac = 0
        print("Entré le nombre de sac\n")
        try:
            nbsac = int(input())
        except Exception:
            print("Erreur de saisie du nombre de sac\n")
            exit(4)
        solution = [0] * self.binPack.nbObjet
        print(f"Saisier pour chaque objet dans quel sac vous le ranger de 0 a {self.binPack.nbObjet - 1}\n")
        try:
            for k in range(self.binPack.nbObjet):
                print(f"L'objet numero {k} ?\n")
                num = int(input())
                if not -1 < num < nbsac:
                    print("Erreur de saisie, le nombre est negatif ou plus grand que le nombre de sac.\n")
                    exit(5)
                solution[k] = num
        except Exception:
            print("Erreur de saisie de la solution")
        certif = Certificat(nbsac, self.binPack.capaciterDunSac, self.binPack.nbObjet, solution)
        print(f"certificat verifier : {certif.certificatCheck(self.binPack.poidsDesObjets)}\n")

    def nonDeterministe(self):
        print("Vous avez choisie Mode : non-deterministe\n")
        print("Entré le nombre de sac\n")
        nbSac = 0
        try:
            nbSac = int(input())
        except Exception:
            print("Erreur de saisie du nombre de sac\n")
            exit(6)
        certifFacto = CertificatFactory(self.binPack.capaciterDunSac, self.binPack.nbObjet)
        certif = certifFacto.genereAleatoireCetificat(nbSac)
        print(
            f"Le certifact genere :\n Nombre d'objet : {certif.nbObjet}, Capaciter d'un sac : {certif.capaciterDunSac},"
            f" Nombre de Sac : {certif.nombreDeSac}\n Solution généré : {certif.solution}\n")
        print(f"Certificat verifier : {certif.certificatCheck(self.binPack.poidsDesObjets)}\n")

    def explorationExaustive(self):
        print("Vous avez choisie Mode : exploration exhaustive")
        print("Choisissez une borne supérieur au nombre de sac\n")
        nbSacMax = 0
        try:
            nbSacMax = int(input())
        except Exception:
            print("Erreur de saisie de la borne supérieur du nombre de sac\n")
            exit(7)
        certifFacto = CertificatFactory(self.binPack.capaciterDunSac, self.binPack.nbObjet)
        certifFacto.setNbSacMaxi(nbSacMax)
        while not certifFacto.isMax():
            certifT = certifFacto.certificatSuivant()
            if certifT.certificatCheck(self.binPack.poidsDesObjets):
                print(
                    f"Un certifact correct:\n Nombre d'objet : {certifT.nbObjet}, Capaciter d'un sac : "
                    f"{certifT.capaciterDunSac}, Nombre de Sac : {certifT.nombreDeSac}\n Solution : {certifT.solution}\n")
                exit(8)
        print("Aucun certicat n'a était trouvé")
